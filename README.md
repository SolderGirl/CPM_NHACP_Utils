# CPM_NHACP_Utils
 
Usage:   

### NHDATE [S]   
  Requests Date and Time from the Server
  and prints it to the console.   
  With the [S] option, it also sets the 
  system clock.
   
### NHDIR   
  List files on the server   
